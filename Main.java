package com.capgemini.marzepka;
import com.google.api.client.http.GenericUrl; 
import com.google.api.client.http.HttpRequest; 
import com.google.api.client.http.HttpRequestFactory; 
import com.google.api.client.http.javanet.NetHttpTransport; 
import org.json.JSONArray; 
import org.json.JSONException; 
import org.json.JSONObject; 
import java.io.IOException; 
public class Main { 
	void run() throws IOException { 
		HttpRequestFactory requestFactory = new NetHttpTransport().createRequestFactory(); 
		HttpRequest request = requestFactory.buildGetRequest( new GenericUrl("https://breaking-bad-quotes.herokuapp.com/v1/quotes")); 
		String rawResponse = request.execute().parseAsString(); 
		JSONArray ja = new JSONArray(rawResponse); 
		JSONObject jo = new JSONObject(ja.get(0).toString()); 
		System.out.println("Quote: "+jo.getString("quote")); 
		System.out.println("~"+jo.getString("author1"));
	} 
	
    public static void main(String[] args) throws IOException { 
        Main m = new Main();
        m.run(); 
	} 	
}
